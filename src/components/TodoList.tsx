import {Component} from 'inferno';
import {Task} from './App';
import './TodoList.scss';

/*
 * This is example of Inferno functional component
 * Functional components provide great performance but does not have state
 */

interface TodoListState {
	tasks: Task[];
}

function sortTasksById(tasks) {
	const stasks = tasks.sort((lhs, rhs) => lhs.id > rhs.id);
	console.log("sorted tasks:", stasks);
	return stasks;
}

export class TodoList extends Component<{}, TodoListState> {
	public componentDidMount(): void {
		fetch('http://localhost:4000/api/todos')
			.then((res) => res.json())
			.then((data) => {
				const sortedTasks = sortTasksById(data.data);
				this.setState({ tasks: sortedTasks });
			})
			// tslint:disable-next-line:no-console
			.catch(console.log);
	}

	public async createTask(desc: string) {
		const response = await fetch('http://localhost:4000/api/todos', {
			body: JSON.stringify({
				todo: {
					description: desc
				}
			}),
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: 'POST'
		});
		const responseBody = await response.json();
		if ('errors' in responseBody) {
			alert('malformed request (see console)');
			// tslint:disable-next-line:no-console
			console.log('malformed request, error: ', responseBody.errors);
		}
		if ('data' in responseBody) {
			this.setState({tasks: sortTasksById(this.state.tasks.concat([responseBody.data as Task]))});
		}
	}

	public async handleTaskToggle(task, ev) {
		const newCheckboxValue = ev.currentTarget.checked;

		const response = await fetch('http://localhost:4000/api/todos/'.concat(task.id.toString()), {
			body: JSON.stringify({
				todo: {
					description: task.description,
					is_complete: (!!newCheckboxValue).toString()
				}
			}),
			headers: {
				'Accept': 'application/json',
					'Content-Type': 'application/json'
			},
			method: 'PUT'
		});

		const responseBody = await response.json();
		if ('errors' in responseBody) {
			alert('malformed request (see console)');
			// tslint:disable-next-line:no-console
			console.log('malformed request, error: ', responseBody.errors);
		}
		if ('data' in responseBody) {
			const currentTasks = this.state.tasks.filter((st) => st.id !== task.id);
			this.setState({tasks: sortTasksById(currentTasks.concat([responseBody.data as Task]))});
		}
	}

	public render() {
		return (
			<div>
				{/* TODO: pull this out into component */}
				<ul>
				{this.state && this.state.tasks ?
					( this.state.tasks.map((task) => {
						return (
							<li className="task">
								<input checked={task.is_complete} data-task-id={task.id}
									type="checkbox"
									onClick={(ev) => this.handleTaskToggle(task, ev)}
								/>
								<label>{task.description}</label>
							</li>
						);
					})) : null
				}
				</ul>
				<form onSubmit={(ev) => {
					ev.preventDefault();
					this.createTask((ev.currentTarget.firstChild as HTMLFormElement).value);
					(ev.currentTarget.firstChild as HTMLFormElement).value = '';
				}}><input type="text" /></form>
			</div>
		);
	}
}
