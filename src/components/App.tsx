import { Component } from 'inferno';
import { TodoList } from './TodoList';

export interface Task {
	id: number;
	description: string;
	is_complete: boolean;
	inserted_at: string;
	updated_at: string;
}

export class App extends Component<{}> {
	constructor(props, context) {
		super(props, context);
	}

	public render() {
		return (
			<div>
				<TodoList />
			</div>
		);
	}
}
