import 'babel-polyfill';
import { Component, render } from 'inferno';
import { App } from './components/App';
import './main.scss';

const container = document.getElementById('app');

class MyComponent extends Component<any, any> {
	constructor(props, context) {
		super(props, context);
	}

	public render() {
		return (
			<div>
				<App />
				{/*	add todo-create component (hooked up to data connect HoC?) */}
				{/* tslint:disable-next-line:indent */}
                {/*	add todo-list component hooked up to data connect HoC */}
			</div>
		);
	}
}

render(<MyComponent />, container);
